FROM alpine:latest as builder

RUN apk add --update --no-cache curl git jq
RUN LATEST=$(curl -s https://api.github.com/repos/docker/buildx/releases/latest | jq -r ".tag_name") && \
    curl -sL --output buildx https://github.com/docker/buildx/releases/download/$LATEST/buildx-$LATEST.linux-amd64 && \
    chmod a+x buildx

FROM docker:stable as deploy

COPY --from=builder buildx /root/.docker/cli-plugins/docker-buildx
RUN apk add --no-cache qemu
